  const defineKindOfTriangle = (sideA, sideB, sideC) => {
    //Your implementation
    if (less(sideA, sideB, sideC)) {
      return "not exists";
    }
  
    if (
      sideEquals(sideA, sideB, sideC) ||
      sideEquals(sideA, sideC, sideB) ||
      sideEquals(sideB, sideC, sideA)
    ) {
      return "rectangular";
    } else if (
      sideMore(sideA, sideB, sideC) ||
      sideMore(sideA, sideC, sideB) ||
      sideMore(sideB, sideC, sideA)
    ) {
      return "obtuse";
    } else if (
      sideLess(sideA, sideB, sideC) &&
      sideLess(sideA, sideC, sideB) &&
      sideLess(sideB, sideC, sideA)
    ) {
      return "acute-angled";
    }
  };
  const sideEquals = (a, b, c) => a * a + b * b === c * c;
  const sideMore = (a, b, c) => a * a + b * b < c * c;
  const sideLess = (a, b, c) => a * a + b * b > c * c;
  const less = (a, b, c) => a + b < c || b + c < a || a + c < c;
  
  module.exports = defineKindOfTriangle;